<%--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>

<%@ page import="org.dspace.core.NewsManager"%>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace"%>

<%
	String topNews = NewsManager.readNewsFile("news-top.html");
%>

<dspace:layout title="Sobre a biblioteca">

<div class="layout-participa col-md-12">
	<div>
		<div class="participa-title">SOBRE A BIBLIOTECA</div>
	</div>
	
	<div>
		<div class="col-md-12 fundo_com_hr">&nbsp;</div>
	</div>
	
	<br/>
	<br/>
	<div class="col-md-12 participa-div-body">
		<p> A Biblioteca Digital de Participação Social é uma plataforma informacional para organização, agregação e disseminação de documentos, publicações e outros conteúdos sobre participação social, movimentos sociais e políticas públicas, dando-lhe maior visibilidade, disponibilidade e acesso permanente. Trata-se de uma iniciativa estratégica para consolidar o tema da Participação Social na sociedade do conhecimento. Em consonância com a missão institucional de promover a Participação Social como Método de Governo, a Secretaria-Geral da Presidência da República contribui para disseminar a gestores, estudantes, acadêmicos, ativistas da sociedade civil e público em geral conhecimento sobre iniciativas democráticas, solidárias, inclusivas e sustentáveis, facilitando, dessa maneira a articulação de uma rede de pessoas e instituições envolvidas com a democratização das relações entre Estado e Sociedade no Brasil. </p>		
	</div>
</div>

</dspace:layout>