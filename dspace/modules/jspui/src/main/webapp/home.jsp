<%--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Home page JSP
  -
  - Attributes:
  -    communities - Community[] all communities in DSpace
  -    recent.submissions - RecetSubmissions
  --%>

<%@page import="org.dspace.content.Bitstream"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.io.File" %>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Locale"%>
<%@ page import="javax.servlet.jsp.jstl.core.*" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.app.webui.components.RecentSubmissions" %>
<%@ page import="org.dspace.content.Community" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.NewsManager" %>
<%@ page import="org.dspace.browse.ItemCounter" %>
<%@ page import="org.dspace.content.DCValue" %>
<%@ page import="org.dspace.content.Item" %>
<%@page import="org.apache.commons.lang.StringUtils"%>


<%
    Locale[] supportedLocales = I18nUtil.getSupportedLocales();
    Locale sessionLocale = UIUtil.getSessionLocale(request);
    Config.set(request.getSession(), Config.FMT_LOCALE, sessionLocale);
    String topNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-top.html"));

    boolean feedEnabled = ConfigurationManager.getBooleanProperty("webui.feed.enable");
    String feedData = "NONE";
    if (feedEnabled)
    {
        feedData = "ALL:" + ConfigurationManager.getProperty("webui.feed.formats");
    }
    
    ItemCounter ic = new ItemCounter(UIUtil.obtainContext(request));

    RecentSubmissions submissions = (RecentSubmissions) request.getAttribute("recent.submissions");
%>

<dspace:layout locbar="nolink" titlekey="jsp.home.title" feedData="<%= feedData %>">


<div class="row">
	<% request.setAttribute("createRootDiv", false); %>
	<%@ include file="discovery/static-sidebar-facet-home.jsp" %>
</div>

<div class="layout-participa col-md-6">


	<div>
		<div class="participa-title">BEM VINDO</div>
	</div>
	
	<div>
		<div class="col-md-12 fundo_com_hr">&nbsp;</div>
	</div>
	
	<div>
		<div style="font-family: Arial, Helvetica, sans-serif; color: #9b9b9c; font-size: 12px; line-height: 150%; text-align: justify;" class="col-md-10">
			<br> Seja bem vindo à Biblioteca Digital da Participação
			Social. Aqui você encontrará as publicações organizadas por
			comunidades, subcomunidades e coleções. Fique à vontade para
			explorar e acessar conteúdos do seu interesse! <br> <br>
			<br> <br>
		</div>
	</div>
</div>

<div class="col-md-1"></div>

<div class="layout-participa col-md-5">
	<div>
		<div class="participa-title">ÚLTIMAS SUBMISSÕES</div>
	</div>
	<div>
		<div class="col-md-12 fundo_com_hr">&nbsp;</div>
	</div>

	<div class="col-md-12 participa-div-body">
		<br/>
		<%
			int i = 0;
			for(Item item : submissions.getRecentSubmissions()) 
			{
				i++;
				if(i == 15)
				{
					break;
				}
				
				DCValue[] dcValues = item.getMetadata("dc", "title", null, "*");
				if(dcValues != null && dcValues.length > 0)
				{
		%>
				- <a href="<%= request.getContextPath() %>/handle/<%= item.getHandle() %>"><%= StringUtils.abbreviate(dcValues[0].value, 65) %></a>
				<br/>
		<%
				}
			}
		%>		
		<br/>
		<b>
			<a href="<%= request.getContextPath() %>/browse?type=dateissued">[+] mais</a>
		</b>
	</div>
</div>
	
</dspace:layout>
